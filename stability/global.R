library("readr")
library("dplyr")
library("reshape2")
library("tidyr")
library("rhandsontable")
library("shinyBS")
library("shinydashboard")
library("shinyjs")
library("V8")
library("markdown")
library("lazyeval")
library("plotly")
library("shinycssloaders")
library("igraph")
library("anytime")
library("Matrix")
library("shinyjs")

source("stability-functions.R")
source("www/my-network.R")

## shinyjs extension to show about modal on startup
showModal <- 'shinyjs.showInfo = function(){$("#aboutModal").modal()}'
