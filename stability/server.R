server <- function(input, output, clientData, session) {
  options(shiny.maxRequestSize=30*1024^2)
  
  # Frieze
  friezeModel <- eventReactive(input$getFrieze, {
    # call frieze.all based on parameters
    
    if (input$directed) {
      result <- frieze.all.directed(t=input$steps,
                                    alpha=input$alpha,
                                    beta=input$beta,
                                    gamma=input$gamma,
                                    delta=input$delta,
                                    p=input$p,
                                    q=input$q,
                                    initial=input$network)$data
    } else {
      result <- frieze.all(t=input$steps,
                           alpha=input$alpha,
                           beta=input$beta,
                           gamma=input$gamma,
                           delta=input$delta,
                           p=input$p,
                           q=input$q,
                           initial=input$network)$data
    }
    
    list(result = result)
  })
  
  # Toggling visability of friezeResults on button click.
  observeEvent(input$getFrieze, {
    shinyjs::toggle("friezeResults")
  })
  
  output$friezeTable <- renderTable (bordered = FALSE, digits = 5, spacing = "l", {
    friezeModel()$result
  })
  
  output$friezePlot <- renderPlotly ({
    if (input$directed) {
      plot.directed(friezeModel()$result, "Block of 10 iterations")$plot
    } else {
      plot.undirected(friezeModel()$result, "Block of 10 iterations")$plot
    }
  })
  
  
  output$downloadData <- downloadHandler(
    filename = function() {
      paste("frieze_data", ".csv", sep = "")
    },
    content = function(file) {
      write.csv(friezeModel()$result, file, row.names = FALSE)
    }
  )
  
  # Finance
  financeModel <- eventReactive(input$getFinance, {
    
    if (input$dataset == "S&P 500") {
      result <- finance.simulator(offs = input$offset,
                        length = input$length,
                        filtered = input$filtered)$data
    } else {
      result <- finance.simulator(offs = input$offset,
                        length = input$length,
                        filtered = input$filtered,
                        sp=FALSE)$data
    }
    
    list(result = result)
  })
  
  # Toggling visability of financeResults on button click.
  observeEvent(input$getFinance, {
    shinyjs::toggle("financeResults")
  })
  
  output$financePlot <- renderPlotly ({
    plot.undirected(financeModel()$result, "Date")$plot
  })
  
  output$financeTable <- renderTable (bordered = FALSE, digits = 5, spacing = "l", {
    financeModel()$result
  })
  
  output$downloadDataFinance <- downloadHandler(
    filename = function() {
      paste("finance_data", ".csv", sep = "")
    },
    content = function(file) {
      write.csv(financeModel()$result, file, row.names = FALSE)
    }
  )
  
  # Sampling
  samplingModel <- eventReactive(input$getSampling, {
    
    result <- sampling.perturbation.simulator(net = input$samplingNet,
                                max.x = input$max.x,
                                min.x = input$min.x,
                                step = input$step,
                                it = input$it,
                                fb = input$fb)$data
    
    list(result = result)
  })
  
  # Toggling visability of samplingResults on button click.
  observeEvent(input$getSampling, {
    shinyjs::toggle("samplingResults")
  })
  
  output$samplingPlot <- renderPlotly ({
    plot.undirected(samplingModel()$result, "Sample size", autorangeType = "reversed")$plot
  })
  
  output$samplingTable <- renderTable (bordered = FALSE, digits = 5, spacing = "l", {
    samplingModel()$result
  })
  
  output$downloadDataSampling <- downloadHandler(
    filename = function() {
      paste("sampling_data", ".csv", sep = "")
    },
    content = function(file) {
      write.csv(samplingModel()$result, file, row.names = FALSE)
    }
  )
}
